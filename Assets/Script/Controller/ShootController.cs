using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    [SerializeField] float maxSpeed = 30f;
    [SerializeField] float speedShoot;
    [SerializeField] bool isTouch = true;
    // Th�ng s? ??u v�o
    [SerializeField] float time = 3f;
    [SerializeField] float angleInRadian;
    [SerializeField] float horizontalVelocity;
    [SerializeField] float verticalVelocity;
    [SerializeField] Rigidbody rb;

    [SerializeField] Vector3 sceenPoint;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector3 curOffset;
    [SerializeField] float maxDistance = 0.2f;
    private void Shoot()
    {
        horizontalVelocity = speedShoot * Mathf.Cos(angleInRadian);
        verticalVelocity = speedShoot * Mathf.Sin(angleInRadian);
        rb.velocity = new Vector3(0, verticalVelocity, horizontalVelocity);
    }
    private void Update()
    {
        if (isTouch)
        {
            ChangeDirectShoot();
            ChangePower();
        }

        if (Input.GetMouseButtonUp(0))
        {
            isTouch = false;
            rb.useGravity = true;
            Shoot();
        }
    }

    private void OnMouseDown()
    {
        if (gameObject.tag == "Weapon")
        {
            isTouch = true;
            rb.useGravity = false;
            ChangeDirectShoot();
        }
    }

    private void ChangeDirectShoot()
    {
        sceenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(sceenPoint.x, sceenPoint.y, sceenPoint.z));
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, sceenPoint.z);
        curOffset = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        if ((float)curOffset.x >= 0.2f) curOffset.x = 0.2f;
        if ((float)curOffset.y >= 0.2f) curOffset.y = 0.2f;
        if ((float)curOffset.x <= 0f) curOffset.x = curOffset.x * -1f;
        if ((float)curOffset.y <= -0.2f) curOffset.y = -0.2f;
        // T�nh g�c ra radian
        angleInRadian = (float)Math.Atan(curOffset.x / -curOffset.y);
        if (angleInRadian <= -1) angleInRadian = 1 + angleInRadian;
        if (angleInRadian >= 1) angleInRadian = 1f;
    }

    private void ChangePower()
    {
        speedShoot = maxSpeed * (curOffset.x / maxDistance); 
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Vector3 offSet;
    [SerializeField] Vector3 direct;
    [SerializeField] Vector3 target;
    [SerializeField] float timeChange = 5f;
    private void Update()
    {
        timeChange -= Time.deltaTime;
        if(timeChange <= 0)
        {
            transform.position = offSet;
            transform.localRotation = Quaternion.Euler(direct);
            timeChange = 0;
        }
    }
}

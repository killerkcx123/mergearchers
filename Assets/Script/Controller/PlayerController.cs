using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject character;
    [SerializeField] Vector3 sceenPoint;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector3 curPoint;
    [SerializeField] Vector3 curOffset;
    [SerializeField] bool isTouch = false;
    [SerializeField] Rigidbody rb;
    private void Update()
    {
        if (isTouch)
        {
            rb.useGravity = false;
            MoveCharacter();
        }

        if (Input.GetMouseButtonUp(0))
        {
            isTouch = false;
            rb.useGravity = true;
        }
    }
    void MoveCharacter()
    {
        sceenPoint = Camera.main.WorldToScreenPoint(character.transform.position);
        offset = character.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(sceenPoint.x, sceenPoint.y, sceenPoint.z));
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, sceenPoint.z);
        curOffset = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        curPoint = new Vector3(curOffset.x, 3f, curOffset.y);
        character.transform.position = curPoint;
    }

    private void OnMouseDown()
    {
        if (gameObject.tag == "Character")
        {
            isTouch = true;
            rb.useGravity = false;
            MoveCharacter();
        }
    }
}

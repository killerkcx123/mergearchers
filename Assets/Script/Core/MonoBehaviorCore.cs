using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourCore : MonoBehaviour
{
    protected SpawnManager sm => SpawnManager.Instance;

    public void RestartCoroutine(ref Coroutine coroutine, IEnumerator enumerator)
    {
        StopOneCoroutine(ref coroutine);
        coroutine = StartCoroutine(enumerator);
    }

    public void StopOneCoroutine(ref Coroutine coroutine)
    {
        if (coroutine != null) StopCoroutine(coroutine);
    }
}
